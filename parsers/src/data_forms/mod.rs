// Copyright (c) 2024 xmpp-rs contributors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

pub use data_form::*;
pub use validate::*;

/// XEP-0004: Data Forms
pub mod data_form;

/// XEP-0122: Data Forms Validation
pub mod validate;
